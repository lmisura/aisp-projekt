#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <queue>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <chrono>

// KOMPARATORI----------------------------------------------------------------------------
struct MaxComp
{
    bool operator()(const std::string &str1, const std::string &str2) const
    {
       
        float float1, float2;

        try
        {
            float1 = std::stof(str1);
            float2 = std::stof(str2);
        }
        catch (const std::exception &e)
        {
           
            std::cerr << "Error converting string to float: " << e.what() << std::endl;
            return false; // Consider them equal in case of conversion error
        }

       
        return float1 < float2; // Change to > for max heap
    }
};

struct MinComp
{
    bool operator()(const std::string &str1, const std::string &str2) const
    {
       
        float float1, float2;

        try
        {
            float1 = std::stof(str1);
            float2 = std::stof(str2);
        }
        catch (const std::exception &e)
        {
       
            std::cerr << "Error converting string to float: " << e.what() << std::endl;
            return false; // Consider them equal in case of conversion error
        }

      
        return float1 > float2; // Change to > for max heap
    }
};
//-----------------------------------------------------------------------------------------------

// KLASA------------------------------------------------------------------------------------------

class Data
{
public:
    // KONSTRUKTOR-----------------------------------------------------------------------
    Data(const std::string &filename) : filename(filename)
    {
       
        std::ifstream file(filename);

        if (!file.is_open())
        {
            std::cerr << "Error opening file!" << std::endl;
        }

        std::string line;
        while (std::getline(file, line))
        {
            std::istringstream iss(line);
            std::string field;
            std::vector<std::string> columns;

           
            while (std::getline(iss, field, ','))
            {
                columns.push_back(field);
            }

            
            if (columns.size() >= 3)
            {
                
                for (int i = 0; i < 3; ++i)
                {
                    dataMap[i].push_back(columns[i]);
                }
            }
        }
     
        file.close();
    }
    //------------------------------------------------------------------------------------------------

    // PRETRAZIVANJE ZAPISA--------------------------------------------------------------

    void PrintData()
    {
        for (int i = 0; i < 3; ++i)
        {
            std::cout << "Column " << i + 1 << " values: ";
            for (const auto &value : dataMap[i])
            {
                std::cout << value << " ";
            }
            std::cout << std::endl;
        }
    }

    void printColumn(int column)
    {
        for (auto value : dataMap[column])
        {
            std::cout << value << std::endl;
        }
    }

    void printColumnRows(int column, int row)
    {
        for (int i = 0; i < row + 1; i++)
        {
            std::cout << dataMap[column][i] << std::endl;
        }
    }

    //-----------------------------------------------------------------------------

    // DOHVACANJE VRIJEDNOSTI

    // Dohvacanje najvece vrijednosti:

    void getTopValue(int column)
    {

        std::priority_queue<std::string, std::vector<std::string>, MaxComp> maxHeap(dataMap[column].begin(), dataMap[column].end());

        std::string maxElement = maxHeap.top();
        std::cout << std::fixed << std::stof(maxElement) << " "; // Print as float with precision
        maxHeap.pop();
    }

    void getTopValues(int column, int n)
    {
        std::priority_queue<std::string, std::vector<std::string>, MaxComp> maxHeap(dataMap[column].begin(), dataMap[column].end());
        for (int i = 0; i < n; i++)
        {
            std::cout << maxHeap.top() << std::endl;
            maxHeap.pop();
        }
    }

    // Dohvacanje najmanje vrijednosti:

    void getBottomValue(int column)
    {
        std::priority_queue<std::string, std::vector<std::string>, MinComp> minHeap(dataMap[column].begin(), dataMap[column].end());

        std::string minElement = minHeap.top();
        std::cout << std::fixed << std::stof(minElement) << " "; // Print as float with precision
        minHeap.pop();
    }

    void getBottomValues(int column, int n)
    {
        std::priority_queue<std::string, std::vector<std::string>, MinComp> minHeap(dataMap[column].begin(), dataMap[column].end());
        for (int i = 0; i < n; i++)
        {
            std::cout << minHeap.top() << std::endl;
            minHeap.pop();
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------

    // BRISANJE ZAPISA-----------------------------------------------------------------------------------------------

    void eraseColumn(int column)
    {
        dataMap.erase(column);
    }

    void eraseColumnRow(int column, int row)
    {
        dataMap[column].erase(dataMap[column].begin(), dataMap[column].begin() + row);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------

    // DODAVANJE ZAPISA

    void insert(int column, std::string zapis)
    {
        dataMap[column].push_back(zapis);
    }

    void insertMany(int column, std::vector<std::string> v)
    {
        for (auto i : v)
        {
            dataMap[column].push_back(i);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------
private:
    std::string filename;
    std::unordered_map<int, std::vector<std::string>> dataMap;
};

int main()
{
    auto start = std::chrono::high_resolution_clock::now();

    Data data("measures_v2.csv");

    //data.PrintData();
    //data.printColumn(0);
    //data.printColumnRows(0,7);
    //data.getBottomValue(0);
    //data.getTopValues(0,7);
    //data.getTopValue(0);
    //getBottomValues(0,7);
    //data.eraseColumn(0);
    //data.eraseColumnRow(0,7);
    //data.insert(0,"400.000");
    //data.insertMany(0,{"1.0","2.0","394.049"});

    auto stop = std::chrono::high_resolution_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(stop -
                                                              start);

    std::cout << duration.count() << " milliseconds.\n";
    
    return 0;
}
