VRIJEME TRAJANJA FUNKCIJA:

- PrintData() = 4840ms
- printColumn(0) = 5742ms
- printColumnRows(0,7) = 3943ms
- getBottomValue(0) = 6416
- getBottomValues(0,7) = 6450ms
- getTopValues(0,7) = 6579ms
- getTopValue(0) = 6626ms
- eraseColumn(0) = 4154ms
- eraseColumnRow(0,7) = 4025ms
- insert(0,"400.000") = 4063ms
- insertMany(0,{"1.0","2.0","394.049"}) = 4276ms


DESKRIPCIJA:

Klasa "Data" ima konstruktor, u konstruktoru se otvara datoteka putem ifstreama te se u njemu citaju podatci iz datoteke te pohranjuju u memoriju.

Struktura koju sam odabrao za pohranjivanje podataka iz datoteke jest "unordered_map" jer se putem nje podatci mogu pohranjivati u "key, value" formatu, te se tako vrlo lako može manipulirati i upravljati redcima i stupcima.


printData()
- Putem ove funkcije se pretražuju svi elementi u datoteci putem for petlje.

printColumn()
- Ova funkcija pretražuje sve elemente unutar jednog odredenog stupca putem for petlje.

printColumnRows()
- Ova funkcija pretražuje n redaka unutar određenog stupca.

getTopValue()
- Putem ove funkcije se dolazi do najveće vrijednosti u stupcu, u funkciji se stvara prioritetni red sa komparatorom koji sortira red od najvećeg prema najmanjem, zatim se samo ispisuje gornji to jest najveći element

getBottomValue()
- Ova funkcija funkcionira isto kao i getTopValue() samo što su komparatori obrnuti.

getTopValues()
- Putem ove funkcije se putem for petlje i prioritetnog reda dolazi do n najvećih vrijednsoti.

getTopValues()
- Putem ove funkcije se putem for petlje i prioritetnog reda dolazi do n najmanjih vrijednsoti.

eraseColumn()
- Briše odabrani stupac pomoću built-in funkcije "erase".

eraseColumnRow()
- Briše prvih n redaka u stupcu, također putem funkcije "erase".

insert()
- Unosi vrijednost u odabrani stupac pomoću "push_back" funkcije.

insertMany()
- Unosi n vrijednosti u određeni stupac također pomoću "push_back" funkcije.
